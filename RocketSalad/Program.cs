﻿using RocketSalad.Filter.Components;
using RocketSalad.Repository;
using RocketSalad.Repository.Configure;
using RocketSalad.Search;
using RocketSalad.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RocketSalad
{
    class Program
    {
        static void Main()
        {
            Search("Shirt", "", 0, 25000);
        }

        static void Search(string q, string made, int minPrice, int maxPrice)
        {
            var items = RepositoryManager.Instance().GetItems();

            var builder = new Builder<Product>(items);
            builder.Add<NameFilter>(q);
            builder.Add<MadeFilter>(made);
            builder.Add<PriceFilter>(minPrice.ToString(), maxPrice.ToString());
            builder.Add<OrderAscFilter>();

            foreach (var item in builder.Build())
            {
                Console.WriteLine("{0}. {1:#,###}원  {2}", item.Id, item.Price, item.Name);
            }
        }
    }
}