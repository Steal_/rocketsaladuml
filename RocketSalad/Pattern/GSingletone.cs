﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Pattern
{
    public class GSingletone<T> where T : class
    {
        private static T _instance;

        public static T Instance()
        {
            if (_instance == null)
            {
                _instance = Activator.CreateInstance<T>();
            }

            return _instance;
        }
    }
}