﻿using RocketSalad.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Search
{
    class Builder<ItemType>
    {
        private IEnumerable<ItemType> datas;
        private IList<IFilter<ItemType>> filters;

        public Builder(IEnumerable<ItemType> datas)
        {
            this.datas = datas;
            this.filters = new List<IFilter<ItemType>>();
        }

        public void Add<T>(params string[] args) where T : IFilter<ItemType>
        {
            T instance = Activator.CreateInstance<T>();

            instance.SetParams(args);

            filters.Add(instance);
        }

        public void Remove<T>() where T : IFilter<ItemType>
        {
            filters = filters
                .Where(f => f.GetType() != typeof(T))
                .ToList();
        }

        public bool Contains<T>()
        {
            return filters.Count(f => f.GetType() == typeof(T)) > 0;
        }

        public IEnumerable<ItemType> Build()
        {
            var enumerator = filters.GetEnumerator();
            enumerator.Reset();

            return _build(datas, enumerator);
        }

        private IEnumerable<ItemType> _build(
            IEnumerable<ItemType> data,
            IEnumerator<IFilter<ItemType>> filters)
        {
            if (filters.MoveNext())
            {
                var result = filters.Current.OnFiltering(data);

                data = _build(result, filters);
            }

            return data;
        }
    }
}