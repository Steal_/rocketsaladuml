﻿using System.Collections.Generic;

namespace RocketSalad.Repository
{
    interface IRepository
    {
        RocketSalad.Repository.Components.DBRepository DBRepository
        {
            get;
            set;
        }

        RocketSalad.Repository.Components.LocalRepository LocalRepository
        {
            get;
            set;
        }
    
        IEnumerable<Product> GetItems();
        IEnumerable<User> GetUsers();
        IEnumerable<Comment> GetComments();

        bool AddCommnet(Comment comment);

        void Init();
    }
}