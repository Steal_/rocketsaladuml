﻿using RocketSalad.Pattern;
using RocketSalad.Repository.Components;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Repository.Configure
{
    public class Bootstrapper : GSingletone<Bootstrapper>
    {
        private Container _container = null;

        public Bootstrapper()
        {
            _container = new Container(c =>
                {
                    c.For<IRepository>().Use<LocalRepository>();
                    //c.For<IRepository>().Use<DBRepository>();
                });
        }
        
        public IContainer Container
        {
            get
            {
                return _container;
            }
        }

        internal RocketSalad.Repository.Components.DBRepository DBRepository
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        internal RocketSalad.Repository.Components.LocalRepository LocalRepository
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
    }
}
