﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RocketSalad.Repository.Components
{
    class DBRepository : IRepository
    {
        public IEnumerable<Product> GetItems()
        {
            // TODO: 실제 구현에선 DB에서 가져와야함

            return File.ReadLines("resource\\data.txt")
                    .Select(line => Product.Parse(line))
                    .Where(p => p != null);
        }

        public IEnumerable<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comment> GetComments()
        {
            throw new NotImplementedException();
        }

        public void Init()
        {
            // TODO: DB Connection ~
        }



        public bool AddCommnet(Comment comment)
        {
            return false;
        }
    }
}
