﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RocketSalad.Repository.Components
{
    class LocalRepository : IRepository
    {
        private List<Product> _items;
        private List<User> _users;
        private List<Comment> _comments;

        public void Init()
        {
            _items = new List<Product>();
            _users = new List<User>();
            _comments = new List<Comment>();

            _items.AddRange(
                File.ReadLines("resource\\data.txt")
                    .Select(line => Product.Parse(line))
                    .Where(p => p != null));

            _users.AddRange(
                File.ReadLines("resource\\users.txt")
                    .Select(line => User.Parse(line))
                    .Where(u => u != null));

            _comments.AddRange(
                File.ReadLines("resource\\comments.txt", Encoding.Default)
                    .Select(line => Comment.Parse(line))
                    .Where(c => c != null));
        }

        public IEnumerable<User> GetUsers()
        {
            return _users;
        }

        public IEnumerable<Comment> GetComments()
        {
            return _comments;
        }

        public IEnumerable<Product> GetItems()
        {
            return _items;
        }


        public bool AddCommnet(Comment comment)
        {
            if (_comments.Contains(comment))
                return false;

            comment.Id = _comments.Count + 1;

            _comments.Add(comment);

            return true;
        }
    }
}