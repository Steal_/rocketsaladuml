﻿using RocketSalad.Pattern;
using RocketSalad.Repository.Configure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Repository
{
    class RepositoryManager : GSingletone<RepositoryManager>
    {
        IRepository repo;

        public RepositoryManager()
        {
            repo = Bootstrapper.Instance().Container.GetInstance<IRepository>();
            repo.Init();
        }

        public RocketSalad.Repository.Configure.Bootstrapper Bootstrapper
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public IEnumerable<User> GetUsers()
        {
            return repo.GetUsers();
        }

        public IEnumerable<Comment> GetComments()
        {
            return repo.GetComments();
        }

        public IEnumerable<Product> GetItems()
        {
            return repo.GetItems();
        }

        public bool AddComment(Comment comment)
        {
            return repo.AddCommnet(comment);
        }
    }
}
