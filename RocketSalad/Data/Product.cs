﻿using RocketSalad.Repository;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace RocketSalad
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Made { get; set; }
        public int Price { get; set; }

        public void WriteComment(User user, string comment, double rate)
        {
            RepositoryManager.Instance().AddComment(
                new Comment()
                {
                    UId = user.Id.ToString(),
                    Content = comment,
                    Rate = rate,
                    Date = DateTime.Now
                });
        }

        public static Product Parse(string line)
        {
            string[] data = Regex.Split(line, "//");

            int id, price;

            if (!int.TryParse(data[0], out id) ||
                !(int.TryParse(data[3], out price)))
                return null;

            return new Product()
            {
                Id = id,
                Name = data[1],
                Made = data[2],
                Price = price
            };
        }

        public override string ToString()
        {
            return string.Join("//",
                new string[]
                {
                    Id.ToString(),
                    Name,
                    Made,
                    Price.ToString()
                });
        }
    }
}