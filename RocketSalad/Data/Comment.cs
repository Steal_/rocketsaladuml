﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RocketSalad
{
    public class Comment
    {
        public int Id { get; set; }
        public string UId { get; set; }
        public string PId { get; set; }
        public string Content { get; set; }
        public double Rate { get; set; }
        public DateTime Date { get; set; }

        public static Comment Parse(string line)
        {
            string[] data = Regex.Split(line, "//");

            int id;
            long dateBin;
            double rate;

            if (!int.TryParse(data[0], out id) || 
                !double.TryParse(data[4], out rate) ||
                !long.TryParse(data[5], out dateBin))
                return null;

            return new Comment()
            {
                Id = id,
                UId = data[1],
                PId = data[2],
                Content = data[3],
                Rate = rate,
                Date = DateTime.FromBinary(dateBin)
            };
        }

        public override string ToString()
        {
            return string.Join("//",
                new string[]
                {
                    Id.ToString(),
                    UId,
                    PId,
                    Content,
                    Rate.ToString(),
                    Date.ToBinary().ToString()
                });
        }
    }
}