﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RocketSalad
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }

        public static User Parse(string line)
        {
            string[] data = Regex.Split(line, "//");

            int id;

            if (!int.TryParse(data[0], out id))
                return null;

            return new User()
            {
                Id = id,
                Username = data[1]
            };
        }

        public override string ToString()
        {
            return string.Join("//", 
                new string[] { Id.ToString(), Username });
        }
    }
}
