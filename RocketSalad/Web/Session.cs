﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Web
{
    class Session : Dictionary<object, object>
    {
        public IEnumerable<Product> GetRecentItems()
        {
            if (!this.ContainsKey("recents"))
                this["recents"] = new Queue<Product>();

            return (Queue<Product>)this["recents"];
        }

        public void AddRecentItem(Product item)
        {
            if (!this.ContainsKey("recents"))
                this["recents"] = new Queue<Product>();

            var stack = (Queue<Product>)this["recents"];

            if (stack.Count >= 3)
                stack.Dequeue();

            stack.Enqueue(item);
        }
    }
}