﻿using RocketSalad.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Web
{
    class SessionManager : GSingletone<SessionManager>
    {
        // 가상 세션
        private List<Session> sessions = new List<Session>()
        {
            new Session(),
            new Session(),
            new Session(),
            new Session()
        };

        public Session CurrentSession
        {
            get
            {
                return sessions[0];
            }
        }
    }
}
