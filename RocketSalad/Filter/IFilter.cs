﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter
{
    interface IFilter<T>
    {
    
        void SetParams(params string[] args);
        IEnumerable<T> OnFiltering(IEnumerable<T> enumerable);
    }
}
