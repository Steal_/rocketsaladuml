﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter.Components
{
    public abstract class BaseFilter<T> : IFilter<T>
    {
        protected string[] args;

        internal RocketSalad.Filter.IFilter<T> IFilter
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }


        public virtual void SetParams(params string[] args)
        {
            this.args = args;
        }

        public abstract IEnumerable<T> OnFiltering(IEnumerable<T> enumerable);
    }
}
