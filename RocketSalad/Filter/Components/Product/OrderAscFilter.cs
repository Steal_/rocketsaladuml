﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter.Components
{
    public class OrderAscFilter : BaseFilter<Product>
    {

        public override IEnumerable<Product> OnFiltering(IEnumerable<Product> enumerable)
        {
            return enumerable
                .OrderBy(p => p.Price);
        }
    }
}
