﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter.Components
{
    public class PriceFilter : BaseFilter<Product>
    {
        int minPrice;
        int maxPrice;

        public override void SetParams(params string[] args)
        {
            base.SetParams(args);

            if (args.Length > 0)
                int.TryParse(args[0], out minPrice);

            if (args.Length > 1)
                int.TryParse(args[1], out maxPrice);
        }

        public override IEnumerable<Product> OnFiltering(IEnumerable<Product> enumerable)
        {
            return enumerable
                .Where(p => minPrice <= p.Price && (maxPrice == 0 || p.Price <= maxPrice));
        }
    }
}
