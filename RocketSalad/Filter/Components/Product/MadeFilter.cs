﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RocketSalad.Filter.Components
{
    public class MadeFilter : BaseFilter<Product>
    {

        public override IEnumerable<Product> OnFiltering(IEnumerable<Product> enumerable)
        {
            return enumerable
                .Where(p => args == null || args.Length == 0 ||
                    Regex.IsMatch(p.Made, args[0]));
        }
    }
}