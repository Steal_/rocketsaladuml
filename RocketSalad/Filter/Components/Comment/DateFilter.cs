﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter.Components
{
    public class DateFilter : BaseFilter<Comment>
    {

        public override IEnumerable<Comment> OnFiltering(IEnumerable<Comment> enumerable)
        {
            return enumerable.OrderBy(c => c.Date);
        }
    }
}