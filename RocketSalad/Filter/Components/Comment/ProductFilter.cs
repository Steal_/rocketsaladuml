﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RocketSalad.Filter.Components
{
    public class ProductFilter : BaseFilter<Comment>
    {
        public override IEnumerable<Comment> OnFiltering(IEnumerable<Comment> enumerable)
        {
            return enumerable
                .Where(c => args == null || args.Length == 0 || c.PId.Equals(args[0]));
        }
    }

}
